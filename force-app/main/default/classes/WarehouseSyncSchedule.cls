global class WarehouseSyncSchedule implements Schedulable{
    // implement scheduled code here
    global void execute(SchedulableContext sc){
        System.debug('*****************************************');
        WarehouseCalloutService.runWarehouseEquipmentSync();
    }

}