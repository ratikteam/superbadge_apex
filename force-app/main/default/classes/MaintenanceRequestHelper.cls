public with sharing class MaintenanceRequestHelper {
    
    public static void updateWorkOrders() {
        // TODO: Complete the method to update workorders
        //bulk determine
        List<Case> maintenanceRequestList = [select id,Subject, case.Vehicle__c,Equipment__c,Equipment__r.Maintenance_Cycle__c,Type,Status  from case where id in :Trigger.New limit 200];
        System.debug('maintenanceRequestList....'+maintenanceRequestList);
        if(maintenanceRequestList !=null && maintenanceRequestList.size()>0 ){
            List<Case> insertMaintenanceRequest = getCaseList(maintenanceRequestList);
            System.debug('insertMaintenanceRequest....'+insertMaintenanceRequest);
            insert insertMaintenanceRequest;
        }
    }        
    
    public static List<Case> getCaseList(List<Case> maintenanceRequestList){
        List<Case> newMaintenanceRequestList = new List<Case>();
        for(Case cas:maintenanceRequestList){
            if( cas.Status=='Closed'){
                case newMaintenanceRequest=new Case();
                newMaintenanceRequest.Subject=cas.Subject+'-'+date.Today();
                newMaintenanceRequest.Type='Routine Maintenance';
                newMaintenanceRequest.Vehicle__c=cas.Vehicle__c;
                newMaintenanceRequest.Equipment__c=cas.Equipment__c;
                newMaintenanceRequest.Date_Reported__c=date.Today();
                newMaintenanceRequest.Date_Due__c=Date.today().addDays(Integer.valueOf(cas.Equipment__r.Maintenance_Cycle__c));
                newMaintenanceRequest.Status='New';
                newMaintenanceRequest.Origin='Phone';
                newMaintenanceRequestList.add(newMaintenanceRequest);
            }
        }
        return newMaintenanceRequestList;
    }
}