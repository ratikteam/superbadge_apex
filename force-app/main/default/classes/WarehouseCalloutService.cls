public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    @future(callout=true)
    public static void runWarehouseEquipmentSync() {
        //ToDo: complete this method to make the callout (using @future) to the
        //      REST endpoint and update equipment on hand.
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(WAREHOUSE_URL);
        HttpResponse response = http.send(request);
        
        if(response.getStatusCode() == 200){
            List<Object> jsonResponse = (List<Object>) JSON.deserializeUntyped(response.getBody());
            system.debug('@Body...'+response.getBody());
            system.debug('@JsonResponse '+jsonResponse);
            
            List<Product2> productList = new List<Product2>();
            for(Object ob : jsonResponse) {
				Map<String,Object> mapJson = (Map<String,Object>)ob;
				Product2 pr = new Product2();
                pr.Name = (String)mapJson.get('name');
                pr.Replacement_Part__c = (Boolean)mapJson.get('replacement');
                pr.Maintenance_Cycle__c = (Integer)mapJson.get('maintenanceperiod');
				pr.Lifespan_Months__c = (Integer)mapJson.get('lifespan');
                pr.Cost__c = (Decimal) mapJson.get('cost');
                pr.Warehouse_SKU__c = (String)mapJson.get('sku');
                pr.Current_Inventory__c = (Double) mapJson.get('quantity');
                productList.add(pr);
            }
            system.debug('@size '+productList.size());
          
            if(productList.size()>0)
                upsert productList Warehouse_SKU__c;

        }
                
    }
}